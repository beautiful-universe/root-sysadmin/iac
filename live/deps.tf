terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }

  backend "s3" {
    bucket = "beautiful-universe-root-iac-terraform"
    key    = "terraform"
    region = "ap-southeast-1"
  }
}
