provider "aws" {
    region     = "ap-southeast-1"
}

module "aws-sso-permission-setup" {
    source = "../modules/aws-sso-permission-setup"
}

module "project-root-sysadmin" {
    source = "../modules/projects/root-sysadmin"
}
