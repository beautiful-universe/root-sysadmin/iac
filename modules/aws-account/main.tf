resource "aws_organizations_account" "account" {
  name  = local.actual_aws_account_name
  email = local.actual_aws_account_email
}
