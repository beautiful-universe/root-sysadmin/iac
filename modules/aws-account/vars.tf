variable "aws_account_name" {
    type = string
    description = "preferred account name, will later be prefixed with beautiful-universe."
}
