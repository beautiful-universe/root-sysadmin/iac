output "aws_account_arn" {
    value = aws_organizations_account.account.arn
}

output "aws_account_id" {
    value = aws_organizations_account.account.id
}
