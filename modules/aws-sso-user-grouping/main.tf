resource "aws_identitystore_group" "idstore-group" {
  identity_store_id = local.aws_ssoadmin_instance_identity_store_id
  display_name      = var.user-grouping-name
  description       = var.user-grouping-description
}

module "sso-userdata" {
  source = "../aws-sso-user-data"
  for_each = toset(var.user-grouping-members)
  aws-sso-user-username = each.key
}

resource "aws_identitystore_group_membership" "user_group_membership" { 
  for_each = toset(values(module.sso-userdata)[*].user_id)
  identity_store_id = local.aws_ssoadmin_instance_identity_store_id
  group_id          = aws_identitystore_group.idstore-group.group_id
  member_id         = each.key
}
