variable "user-grouping-name" {
    type = string
    description = "sso group name"
}

variable "user-grouping-description" {
    type = string
    description = "sso group description"
}

variable "user-grouping-members" {
    type = list(string)
    description = "sso usernames to be a member of this user grouping"
}
