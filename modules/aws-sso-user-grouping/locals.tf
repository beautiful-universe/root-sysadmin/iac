data "aws_ssoadmin_instances" "sso_instance" {}

locals {
    aws_ssoadmin_instance_identity_store_id = tolist(data.aws_ssoadmin_instances.sso_instance.identity_store_ids)[0]
    aws_ssoadmin_instance_arn = tolist(data.aws_ssoadmin_instances.sso_instance.arns)[0]
}
