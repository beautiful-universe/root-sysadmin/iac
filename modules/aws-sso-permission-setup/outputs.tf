output "aws_ssoadmin_permission_set_admin_user_id" {
    value = module.aws-sso-permission-setup-admin-user.aws_ssoadmin_permission_set_id
}

output "aws_ssoadmin_permission_set_admin_user_arn" {
    value = module.aws-sso-permission-setup-admin-user.aws_ssoadmin_permission_set_arn
}
