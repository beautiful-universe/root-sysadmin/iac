data "aws_iam_policy_document" "admin_user_iam_policy" {
  statement {
    sid = ""
    actions = [
      "*",
    ]
    effect = "Allow"
    resources = [
      "*",
    ]
  }
}

module "aws-sso-permission-setup-admin-user" {
  source = "../aws-sso-permission-template"
  sso_permission_template_name = "admin_user"
  sso_permission_template_description = "Super Admin of assigned AWS Accounts"
  sso_permission_template_session_duration = "PT8H"
  sso_permission_template_iam_policy_document_json = data.aws_iam_policy_document.admin_user_iam_policy.json
}
