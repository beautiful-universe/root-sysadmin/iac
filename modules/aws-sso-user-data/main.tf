data "aws_identitystore_user" "sso_user" {
  identity_store_id = local.aws_ssoadmin_instance_identity_store_id

  alternate_identifier {
    unique_attribute {
      attribute_path  = "UserName"
      attribute_value = var.aws-sso-user-username
    }
  }
}
