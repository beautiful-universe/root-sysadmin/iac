resource "aws_ssoadmin_permission_set" "permission_set" {
  name = var.sso_permission_template_name
  description = var.sso_permission_template_description
  instance_arn = local.aws_ssoadmin_instance_arn
  session_duration = var.sso_permission_template_session_duration
}

resource "aws_ssoadmin_permission_set_inline_policy" "power_user_permission_set_policy" {
  inline_policy      = var.sso_permission_template_iam_policy_document_json
  instance_arn       = local.aws_ssoadmin_instance_arn
  permission_set_arn = aws_ssoadmin_permission_set.permission_set.arn
}
