output "aws_ssoadmin_permission_set_id" {
    value = aws_ssoadmin_permission_set.permission_set.id
}

output "aws_ssoadmin_permission_set_arn" {
    value = aws_ssoadmin_permission_set.permission_set.arn
}
