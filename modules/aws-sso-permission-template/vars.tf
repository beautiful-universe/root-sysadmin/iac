variable "sso_permission_template_name" {
  type = string
  description = "aws sso permission template name"
}

variable "sso_permission_template_description" {
  type = string
  description = "aws sso permission template desc."
}

variable "sso_permission_template_session_duration" {
  type = string
  description = "aws sso permission template duration"
  default = "PT1H"
}

variable "sso_permission_template_iam_policy_document_json" {
  type = string
  description = "aws sso permission template iam policy document in json"
}

