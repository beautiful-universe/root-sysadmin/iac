data "aws_organizations_organization" "aws-org" {}

data "aws_iam_policy_document" "sysadmin-user-iam-policy" {
  statement {
    sid = ""
    actions = [
      "*",
    ]
    effect = "Allow"
    resources = [
      "*",
    ]
  }
}

module "aws-sso-permission-setup-sysadmin-user" {
    source = "../../aws-sso-permission-template"
    sso_permission_template_name = "sysadmin-user"
    sso_permission_template_description = "sysadmin user for root aws account"
    sso_permission_template_session_duration = "PT8H"
    sso_permission_template_iam_policy_document_json = data.aws_iam_policy_document.sysadmin-user-iam-policy.json
}

module "aws-sso-sysadmin-user-grouping" {
    source = "../../aws-sso-user-grouping"
    user-grouping-name = "sysadmin-user-grouping"
    user-grouping-description = "aws sso user grouping for sys admin"
    user-grouping-members = [
        "fatih"
    ]
}

resource "aws_ssoadmin_account_assignment" "sysadmin-user-account-assignment" {
  instance_arn       = local.aws_ssoadmin_instance_arn
  permission_set_arn = module.aws-sso-permission-setup-sysadmin-user.aws_ssoadmin_permission_set_arn

  principal_id   = module.aws-sso-sysadmin-user-grouping.group_id
  principal_type = "GROUP"

  target_id   = data.aws_organizations_organization.aws-org.master_account_id
  target_type = "AWS_ACCOUNT"
}
